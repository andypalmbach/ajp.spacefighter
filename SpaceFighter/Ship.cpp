
#include "Ship.h"

// The following methods outlined here set the basic behavior for every ship in the game 

Ship::Ship()
{
	// Sets the initial position of the Ship object and the size of the ship's "hitbox" (collision radius)
	SetPosition(0, 0);
	SetCollisionRadius(10);

	// Sets the ship's speed, maximum hit points, and sets the ship as vulnerable to damage by default
	m_speed = 300;
	m_maxHitPoints = 3;
	m_isInvulnurable = false;

	// Generates an instance of a "ship" - constructor function
	Initialize();
}

// this method handles updates having to do with the ship's weapon use
void Ship::Update(const GameTime *pGameTime)
{
	// initializes the ship's weapon 
	m_weaponIt = m_weapons.begin();
	// checks to make sure that the weapon is firing and has not reached the end of the weapon's use, and increments a counter for weapon use
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		// updates the ship's 
		(*m_weaponIt)->Update(pGameTime);
	}

	// do any in-game updates that a normal game object would have
	GameObject::Update(pGameTime);
}

// this method handles the behavior of ships when they get hit by weapon fire
void Ship::Hit(const float damage)
{
	// check to see if the ship is vulnerable to damage
	if (!m_isInvulnurable)
	{
		// decrements the value of incoming damage from the ship's hit points
		m_hitPoints -= damage;

		//removes the ship if the hit points of the ship drop to 0 or lower
		if (m_hitPoints <= 0)
		{
			GameObject::Deactivate();
		}
	}
}

// this method sets a new ship's initial hit points to the default max hit points
void Ship::Initialize()
{
	m_hitPoints = m_maxHitPoints;
}

// this method handles the behavior of ships firing their weapons
void Ship::FireWeapons(TriggerType type)
{
	// initializes the ship's weapon
	m_weaponIt = m_weapons.begin();
	// checks to make sure that the weapon is firing and has not reached the end of the weapon's use, and increments a counter for weapon use
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		// fires the ship's weapon
		(*m_weaponIt)->Fire(type);
	}
}

void Ship::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);
}